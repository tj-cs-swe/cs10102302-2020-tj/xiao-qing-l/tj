# 引入path
from django.urls import path
from . import views

# 正在部署的应用的名称
app_name = 'blocks'

urlpatterns = [
    path('blocks-list-friend/', views.blocks_list_friend, name='blocks_list_friend'),
    path('blocks-list-study/', views.blocks_list_study, name='blocks_list_study'),
    path('blocks-list-life/', views.blocks_list_life, name='blocks_list_life'),
]