from django.http import HttpResponse,StreamingHttpResponse
from django.shortcuts import render, redirect
import markdown
from comment.models import Comment

from article.models import ArticleColumn  # 文章板块
from article.models import ArticlePost
from article.forms import ArticlePostForm

from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from comment import views as comment_views
# 引入评论表单
from comment.forms import CommentForm
# 引入 Q 对象
from django.db.models import Q
from django.conf import settings
from django.utils.http import urlquote
import os
# 引入分页模块
from django.core.paginator import Paginator
#引入权限检查模块
from django.contrib.auth.models import Permission

from django.shortcuts import render, redirect, reverse
from question.models import QuestionPost, QuestionColumn
from question.forms import QuestionPostForm
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.contrib import messages
from answer.models import Answer
from django.http import HttpResponse,StreamingHttpResponse
from django.utils.http import urlquote
from article import views as article_views
from question import views as question_views
# 引入分页模块
import markdown

""" 板块列表

    在指定的板块内显示文章和问题，同时实现搜索。

        属性：
            request     ：原视图函数传入请求
            column      ：板块（注意，此处需传入int型，对应后台该板块的编号）
            html        ：需要渲染的模板（str型）
"""
def block_list(request, column, html):
    search = request.GET.get('search')
    articles = ArticlePost.objects.filter(column=column)
    questions = QuestionPost.objects.filter(column=column)
    if search:
        questions = questions.filter(
            # 匹配标题(不区分大小写)
            Q(title__icontains=search) |
            # 匹配问题描述(不区分大小写)
            Q(description__icontains=search)
        ).order_by('-updated')
        articles = articles.filter(
            # 匹配标题(不区分大小写)
            Q(title__icontains=search) |
            # 匹配问题描述(不区分大小写)
            Q(body__icontains=search)
        ).order_by('-updated')

    context = {'questions': questions, 'articles': articles, 'search': search}
    return render(request, html, context)


# 视图函数
def blocks_list_friend(request):
    return block_list(request, 2, 'blocks/list_friend.html')


def blocks_list_study(request):
    return block_list(request, 0, 'blocks/list_study.html')


def blocks_list_life(request):
    return block_list(request, 1, 'blocks/list_life.html')