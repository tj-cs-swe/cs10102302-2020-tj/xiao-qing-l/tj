#!/usr/bin/env python 
# -*- coding:utf-8 -*-
from django.urls import path
from . import views

app_name = 'collect'

urlpatterns = [
    path('collect-change/', views.collect_change, name='collect_change'),
]