from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.auth.models import User


class CollectRecord(models.Model):
    """收藏记录

    每个用户对每个文章/问题/回答收藏，都会实例化一个ColletRecord；
    每个用户对每个文章/问题/回答取消收藏，都会删除对应的ColletRecord。

    属性：
        content_type   ：关联对象的类型
        object_id      ：关联对象的唯一id
        content_object ：由content_type和object_id共同确定的外键，和唯一确定的对象关联
        user           ：收藏的用户
        collected_time ：收藏的时间
    """
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey("content_type", "object_id")

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    collected_time = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-collected_time']
