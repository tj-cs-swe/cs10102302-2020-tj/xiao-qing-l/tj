from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.urls import reverse
from django.contrib.contenttypes.fields import GenericRelation
from like.models import LikeRecord
from collect.models import CollectRecord
from taggit.managers import TaggableManager


# 提问板块模型
class QuestionColumn(models.Model):
    # 板块标题
    title = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return self.title


# 博客问题数据模型
class QuestionPost(models.Model):
    # 问题作者。
    author = models.ForeignKey(User, on_delete=models.CASCADE)

    # 问题版块。
    column = models.ForeignKey(
        QuestionColumn,
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name='question'
    )

    # 问题标签
    forum = TaggableManager(blank=True)

    # 问题标题。
    title = models.CharField(max_length=100)

    # 问题描述。
    description = models.TextField(max_length=500)

    # 问题创建时间。参数 default=timezone.now 指定其在创建数据时将默认写入当前的时间
    created = models.DateTimeField(default=timezone.now)

    # 问题更新时间。参数 auto_now=True 指定每次数据更新时自动写入当前时间
    updated = models.DateTimeField(auto_now=True)

    # 上传文件
    file = models.FileField(upload_to='question_file/%Y%m%d/', blank=True)

    # 点赞记录
    like_records = GenericRelation(LikeRecord, related_query_name='articles')

    # 收藏记录
    collect_records = GenericRelation(CollectRecord, related_query_name='questions')
    
    # 浏览量
    total_views = models.PositiveIntegerField(default=0)

    class Meta:
        # ordering 指定模型返回的数据的排列顺序
        # '-created' 表明数据应该以创建时间的倒序排列
        ordering = ('-created',)

    def __str__(self):
        # 将问题标题返回
        return self.title
    
    # 获得问题详细页面url
    def get_absolute_url(self):
        return reverse('question:question_detail', args=[self.id])

    # 问题是否收藏
    def question_is_collected(self, user):
        collect_records = self.collect_records.filter(user=user)
        return len(collect_records) != 0

    # 问题是否点赞
    def question_is_liked(self, user):
        like_records = self.like_records.filter(user=user)
        return len(like_records) != 0

    # 问题创建时间是否错误
    def was_created_recently(self):
        diff = timezone.now() - self.created
        
        if diff.days == 0 and diff.seconds >= 0 and diff.seconds < 60:
            return True
        else:
            return False

    # 问题更新时间是否错误
    def was_updated_recently(self):
        diff = timezone.now() - self.updated
        
        if diff.days == 0 and diff.seconds >= 0 and diff.seconds < 60:
            return True
        else:
            return False