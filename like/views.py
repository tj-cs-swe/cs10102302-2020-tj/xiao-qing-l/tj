from django.contrib.contenttypes.models import ContentType
from django.http import JsonResponse

from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from like.models import LikeRecord


# 函数名：  success_response
# 作者：    ny
# 日期：    2020-7-19
# 功能：    返回成功状态标识及点赞数量和状态的json响应
# 输入参数：likes_num 该文章的点赞数量
#           is_liked 该用户是否给该文章点赞
# 返回值：  类型（JsonResponse）
#          'status'   : 'SUCCESS' 成功
#          'likes_num': likes_num 该文章的点赞数量
#          'is_liked' : is_liked  该用户是否给该文章点赞
# 修改记录：
def success_response(likes_num, is_liked):
    data = {'status': 'SUCCESS', 'likes_num': likes_num, 'is_liked': is_liked}
    return JsonResponse(data)


# 函数名：  error_response
# 作者：    ny
# 日期：    2020-7-19
# 功能：    返回失败状态标识、错误码和错误信息的json响应
# 输入参数：code 错误码
#           message 错误信息
# 返回值：  类型（JsonResponse）
#          'status' : 'ERROR' 失败
#          'code'   : code    错误码
#          'message': message 错误信息
# 修改记录：
def error_response(code, message):
    data = {'status': 'ERROR', 'code': code, 'message': message}
    return JsonResponse(data)


# 函数名：  get_likes_num
# 作者：    ny
# 日期：    2020-7-19
# 功能：    返回type和id唯一确定的文章/问题/回答的当前点赞数量
# 输入参数：content_type model类型
#           object_id   model实例的唯一id
# 返回值：  类型（int）
#          返回type和id唯一确定的文章/问题/回答的当前点赞数量
# 修改记录：
def get_likes_num(content_type, object_id):
    like_records = LikeRecord.objects.all().filter(content_type=content_type, object_id=object_id)
    likes_num = len(like_records)
    return likes_num


# 函数名：  like_change
# 作者：    ny
# 日期：    2020-7-19
# 功能：    返回成功状态/失败状态的json响应
# 输入参数：request Django前端传来的请求
# 返回值：  类型（JsonResponse）
#          返回SUCCESS标识的json响应，说明点赞成功，并同时返回点赞数量及状态
#          返回ERROR标识的json响应，说明点赞失败，并同时返回错误码和错误信息
# 修改记录：
@login_required(login_url='/userprofile/login/')
def like_change(request):
    # 判断该用户是否有相关权限
    myuser = User.objects.get(id=request.user.id)
    if not myuser.has_perm('like.change_likecount'):
        return HttpResponse('抱歉，您的相关权限已被管理员封禁；\n如有疑问，请联系管理员！')
    
    # 获取发起请求的对象信息
    user = request.user
    content_type = request.GET.get('content_type')
    content_type = ContentType.objects.get(model=content_type)
    object_id = request.GET.get('object_id')

    # 若未点赞，则进行点赞，创建点赞记录
    # 若已点赞，则取消点赞，删除点赞记录
    like_record, created = LikeRecord.objects.get_or_create(content_type=content_type, object_id=object_id, user=user)
    if not created:
        like_record.delete()

    likes_num = get_likes_num(content_type, object_id)
    is_liked = True if created else False
    return success_response(likes_num, is_liked)
